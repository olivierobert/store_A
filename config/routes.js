module.exports.routes = {

  'GET /products': {
    controller: 'Product',
    action: 'index',
  },
  'POST /product': {
    controller: 'Product',
    action: 'create',
  },
  'GET /product/:id': {
    controller: 'Product',
    action: 'find',
  },
  'PUT /product/:id?': {
    controller: 'Product',
    action: 'update',
  },
  'DELETE /product/:id?': {
    controller: 'Product',
    action: 'destroy',
  },

}
