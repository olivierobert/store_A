module.exports = {

  port: 3000,

  connections: {
    localDiskDb: {
      adapter: 'sails-disk'
    }
  },

  models: {
    connection: 'localDiskDb',
    migrate: 'alter',
    schema: true,
    autoPK: true
  },

  blueprints: {
    actions: false,
    rest: false,
    shortcuts: false,
    // prefix: '',
    // restPrefix: '',
    pluralize: false,
    // populate: true,
    // autoWatch: true,
    // defaultLimit: 30
    index: false
  },

  // i18n: {
  //   locales: ['ar', 'en'],
  //   defaultLocale: 'en',
  //   updateFiles: false,
  //   localesDirectory: '/config/locales'
  // },

  // csrf: {
  //   grantTokenViaAjax: true,
  //   origin: ''
  // },

  cors: {
    allRoutes: true,
    origin: '*',
    credentials: true,
    // methods: 'GET, POST, PUT, DELETE, OPTIONS, HEAD',
    // headers: 'content-type'
  },

  // globals: {
  //   _: true,
  //   async: true,
  //   sails: true,
  //   services: true,
  //   models: true
  // },

  // log: {
  //   level: 'info'
  // },

  // policies: {
  //   '*': true,
  //   RabbitController: {
  //     '*': false,
  //     nurture : 'isRabbitMother',
  //     feed : ['isNiceToAnimals', 'hasRabbitFood']
  //   }
  // },

};