var Dispatcher = require('../dispatchers/appDispatcher'),
    Constants = require('../constants/appConstants'),
    StoreAbstraction = require('../helpers/storeAbstraction'),
    assign = require('object-assign'),
    EventEmitter = require('events').EventEmitter,
    CHANGE_EVENT = 'change'

var AppStore = assign(EventEmitter.prototype, {
  emitChange: function() {
    this.emit(CHANGE_EVENT)
  },
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback)
  },
  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback)
  },
  getCart: function() {
    return StoreAbstraction._cartItems
  },
  getCatalog: function() {
    return StoreAbstraction._catalog
  },
  getProduct: function(id) {
    return StoreAbstraction._product
  },
  getCartTotals: function() {
    return StoreAbstraction._cartTotals()
  },
})

dispatcherIndex: Dispatcher.register(function(payload) {
  var action = payload.action
  switch (action.actionType){
    case Constants.RECEIVE_PRODUCTS: StoreAbstraction._catalog = action.catalog
      break
    case Constants.GET_PRODUCT:      StoreAbstraction._product = action.product
      break
    case Constants.ADD_ITEM:         StoreAbstraction._addItem(action.item)
      break
    case Constants.REMOVE_ITEM:      StoreAbstraction._removeItem(action.index)
      break
    case Constants.INCREASE_ITEM:    StoreAbstraction._increaseItem(action.index)
      break
    case Constants.DECREASE_ITEM:    StoreAbstraction._decreaseItem(action.index)
      break
  }
  AppStore.emitChange()
  return true
})

module.exports = AppStore
