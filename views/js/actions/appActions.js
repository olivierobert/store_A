var AppConstants = require('../constants/appConstants'),
    AppDispatcher = require('../dispatchers/appDispatcher')

module.exports = {
  receiveProducts:function(products) {
    AppDispatcher.handleServerAction({
      actionType: AppConstants.RECEIVE_PRODUCTS,
      catalog: products
    })
  },
  getProductItem:function(product) {
    AppDispatcher.handleServerAction({
      actionType: AppConstants.GET_PRODUCT,
      product: product
    })
  },
  addItem: function(item) {
    AppDispatcher.handleViewAction({
      actionType: AppConstants.ADD_ITEM,
      item: item
    })
  },
  removeItem: function(index) {
    AppDispatcher.handleViewAction({
      actionType: AppConstants.REMOVE_ITEM,
      index: index
    })
  },
  increaseItem: function(index) {
    AppDispatcher.handleViewAction({
      actionType: AppConstants.INCREASE_ITEM,
      index: index
    })
  },
  decreaseItem: function(index) {
    AppDispatcher.handleViewAction({
      actionType: AppConstants.DECREASE_ITEM,
      index: index
    })
  },
}
