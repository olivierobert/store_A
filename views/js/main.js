var App = require('./components/app'),
    React = require('react'),
    StoreApi = require('./helpers/storeApi')

StoreApi.getProducts()

React.render(<App />, document.getElementById('main'))
