module.exports = {
  ADD_ITEM: 'ADD_ITEM',
  REMOVE_ITEM: 'REMOVE_ITEM',
  INCREASE_ITEM: 'INCREASE_ITEM',
  DECREASE_ITEM: 'DECREASE_ITEM',
  RECEIVE_PRODUCTS: 'RECEIVE_PRODUCTS',
  GET_PRODUCT: 'GET_PRODUCT'
}
