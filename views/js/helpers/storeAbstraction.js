'use strinct'

module.exports = {

  _catalog: [],
  _product: [],
  _cartItems: [],

  // _removeItem: function(index) {
  //   this._cartItems[index].inCart = false
  //   this._cartItems.splice(index, 1)
  // },

  // _increaseItem: function(index) {
  //   this._cartItems[index].qty++
  // },

  _decreaseItem: function(index) {
    if (this._cartItems[index].qty > 0) {
      this._cartItems[index].qty--
    } 
    // else {
    //   // this._removeItem(index)
    //   this._cartItems[index].inCart = false
    //   this._cartItems.splice(index, 1)
    // }
  },

  _addItem: function(item) {
    if (!item.inCart) {
      item['qty'] = 1
      item['inCart'] = true
      this._cartItems.push(item)
    } else {
      var self = this
      this._cartItems.forEach(function(cartItem, i) {
        if (cartItem.id === item.id) {
          self._cartItems[i].qty++
        }
      })
    }
  },

  _cartTotals: function() {
    var qty = 0
    var total = 0
    this._cartItems.forEach(function(cartItem) {
      qty += cartItem.qty
      total += cartItem.qty * cartItem.sale_price
    })
    return {'qty': qty, 'total': total}
  },

}
