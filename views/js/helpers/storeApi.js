var jQuery = require('jquery'),
    Actions = require('../actions/appActions')

module.exports = {
  getProducts: function() {
    jQuery.ajax({
      url: 'http://localhost:3000/products',
      success: Actions.receiveProducts,
      error: Actions.receiveProducts('error')
    })
  },
  getProductItem: function(id) {
    jQuery.ajax({
      url: 'http://localhost:3000/product/'+id,
      success: Actions.getProductItem,
      error: Actions.getProductItem('error')
    })
  }
}
