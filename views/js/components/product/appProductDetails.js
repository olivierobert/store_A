var React = require('react'),
    Store = require('../../stores/appStore'),
    AddToCart = require('../catalog/appAddToCart'),
    StoreWatchMixin = require('../../mixins/StoreWatchMixin'),
    CatalogItem = require('../catalog/appCatalogItem'),
    StoreApi = require('../../helpers/storeApi'),
    Link = require('react-router-component').Link
    require('events').EventEmitter.prototype._maxListeners = 300

function getProductItem(component) {
  return { item: Store.getProduct(component.props.item) }
}

var CatalogDetails = React.createClass({
  mixins: [StoreWatchMixin(getProductItem)],
  componentDidMount: function() {
    Store.addChangeListener(this._onChange)
    StoreApi.getProductItem(this.props.item)
  },
  render: function() {
    var item = this.state.item
    console.log(item)
    return (
      <div>
        <h2>{item.title}</h2>
        <img src={item.image} alt="" />
        <p>{item.summary}</p>
        <p>${item.sale_price} <span className="text-success">{item.inCart && '(' + item.qty + ') in cart'}</span></p>
        <div className="btn-group btn-group-sm">
          <AddToCart item={item} />
          <Link href='/' className="btn btn-default">Continue Shopping</Link>
        </div>
      </div>
    )
  }
})

module.exports = CatalogDetails
