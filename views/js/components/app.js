var React = require('react'),
    Catalog = require('./catalog/appCatalog'),
    Cart = require('./cart/appCart'),
    Router = require('react-router-component'),
    ProductDetails = require('./product/appProductDetails'),
    Template = require('./appTemplate'),
    Locations = Router.Locations,
    Location = Router.Location

var App = React.createClass({
  render: function() {
    return (
      <Template>
        <Locations>
          <Location path="/" handler={Catalog} />
          <Location path="/cart" handler={Cart} />
          <Location path="/item/:item" handler={ProductDetails} />
        </Locations>
      </Template>
    )
  }
})

module.exports = App
