var React = require('react'),
    Link = require('react-router-component').Link,
    AddToCart = require('./appAddToCart')

var CatalogItem = React.createClass({
  render: function() {
    var item = this.props.item,
        itemStyle = {
          borderBottom: '1px solid #ccc',
          paddingBottom: 15
        }
    return (
      <div className="col-sm-3" style={itemStyle}>
        <h4>{item.title}</h4>
        <img src={item.image} alt="" />
        <p>{item.summary}</p>
        <p>${item.sale_price}<span className="text-success"> {item.inCart && '(' + item.qty + ') in cart'}</span></p>
        <div className="btn-group btn-group-xs">
          <Link href={'/item/' + item.id} className="btn btn-default">Learn More</Link>
          <AddToCart item={item} />
        </div>
      </div>
    )
	}
})

module.exports = CatalogItem
