var React = require('react'),
    Store = require('../../stores/appStore'),
    AddToCart = require('./appAddToCart'),
    StoreWatchMixin = require('../../mixins/StoreWatchMixin'),
    CatalogItem = require('../catalog/appCatalogItem'),
    Actions = require('../../actions/appActions')

function getCatalog() {
  return { items: Store.getCatalog() }
}

var Catalog = React.createClass({
  mixins: [StoreWatchMixin(getCatalog)],
  componentDidMount: function() {
    Store.addChangeListener(this._onChange)
  },
  render: function() {
    var items = 'Loading ..'
    if(this.state.items !== 'error') {
      items = this.state.items.map(function(item){
        return <CatalogItem key={item.id} item={item} />
      })
    }
    return (
      <div className="row">
        {items}
      </div>
    )
  }
})

module.exports = Catalog
