# NoSQL Sails React Node


## Installation
@ root path of this application
```
$ npm install
```
```
$ gulp
```
```
$ sails lift
```
## Architecture
Its an [Sails](http://sailsjs.org) application

/api

  |----> Containe models and controllers, CRUD Rest application/json

/views

  |----> Containe [React/Flux](https://facebook.github.io/flux/docs/overview.html) architecture

When run ```$ gulp``` /src are compiled & build into /assets

see /gulpfile.js for this function.

## Model
Currently, we storing data on HardDisk using [sails-disk](https://github.com/balderdashy/sails-disk)
