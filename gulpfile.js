var gulp = require('gulp');
var browserify = require('browserify');
var reactify = require('reactify');
var source = require('vinyl-source-stream');

gulp.task('browserify', function() {
  browserify('views/js/main.js')
    .transform('reactify')
    .bundle()
    .pipe(source('main.js'))
    .pipe(gulp.dest('assets/js'));
});

gulp.task('copy', function() {
  gulp.src('views/index.html')
    .pipe(gulp.dest('assets'));
  gulp.src('views/images/**/*.*')
    .pipe(gulp.dest('assets/images'));
});

gulp.task('default',['browserify', 'copy'], function() {
  return gulp.watch('views/**/*.*', ['browserify', 'copy']);
});
